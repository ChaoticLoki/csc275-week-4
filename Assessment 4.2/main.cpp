#include "StandardIncludes.h"
#include "SieveOfEratosthenes.h"
#define SIZE	1023

int main()
{
	std::cout << "Printing Primes from 2 to " << SIZE << std::endl;
	//int sieveArray[SIZE];
	SieveOfEratosthenes sieve(SIZE);
	int input;
	do
	{
		std::cout << "Prime Checker [1 or 0 to exit]:";
		std::cin >> input;
		if(input == 0 || input == 1)
			break;
		if(sieve.isPrime(input))
		{
			std::cout << input << " is a prime!" << std::endl;
		}
		else
		{
			std::cout << input << " is not a prime!" << std::endl;
			std::cout << "Prime Factors: "; 
			sieve.printPrimeFactors(input); 
			std::cout << std::endl;
		}
	}while(input != 0 || input != 1);
}