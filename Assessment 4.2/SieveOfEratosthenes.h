#pragma once
#include "StandardIncludes.h"

class SieveOfEratosthenes
{
public:
	SieveOfEratosthenes(int);
	~SieveOfEratosthenes();
	void fillArray(int);
	void zeroNonPrimeNumbers(int);
	void outputArray(int);
	bool isPrime(int);
	void printPrimeFactors(int);
private:
	//bool *sieveArray;
	std::bitset<1024> bitSieve;
};