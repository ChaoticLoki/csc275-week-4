#include "SieveOfEratosthenes.h"
#include "StandardIncludes.h"

SieveOfEratosthenes::SieveOfEratosthenes(int max)
{
	//sieveArray = new bool[max];
	// TODO Auto-generated constructor stub
	fillArray(max);
	zeroNonPrimeNumbers(max);
	outputArray(max);

}

SieveOfEratosthenes::~SieveOfEratosthenes()
{
	// TODO Auto-generated destructor stub
	//delete[] sieveArray;
}

void SieveOfEratosthenes::fillArray(int max)
{
	//Initialize Array with true
	for (int index = 2; index <= max; index++)
	{
		bitSieve[index] = 1;
		//sieveArray[index] = 1;
	}
}

void SieveOfEratosthenes::zeroNonPrimeNumbers(int max)
{
	//Iterate each number and remove all of its factors
	for (int divider = 2; (divider * divider) <= max; divider++)
	{
		//If the element is false then it is not a prime and all factors have already been zeroed and program can skip this check
		//If the element is true, then it is a prime number
		//if (!sieveArray[divider])
		if (!bitSieve[divider])
			continue;
		//Zero all factors of divider
		//Used divider*divider because all factors of divider prior to the square of divider is a factor of previous primes. Not sure if that makes sense or not.
		for (int index = (divider * divider); index <= max; index = index + divider)
		{
			bitSieve[index] = 0;
			//sieveArray[index] = 0;
		}
	}
}

void SieveOfEratosthenes::outputArray(int max)
{
	for (int index = 2; index < max; index++)
	{
		//if (sieveArray[index])
		if (bitSieve[index])
		{
			std::cout << index << std::endl;
		}
	}
}

bool SieveOfEratosthenes::isPrime(int index)
{
	return bitSieve[index];
}

void SieveOfEratosthenes::printPrimeFactors(int numberToGetFactors)
{
	for(int index = 2; index < numberToGetFactors; index++)
	{
		//No point checking if it is zeroed because it is not a prime
		if (!bitSieve[index])
			continue;
		//Check if input factors into index. If it does, it is a factor.
		if (numberToGetFactors % index == 0)
		{
			std::cout << index << " ";
		}
	}
}