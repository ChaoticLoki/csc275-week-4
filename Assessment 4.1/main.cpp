#include "StandardIncludes.h"
#include "Palindrome.h"

void numberPalindrome()
{
	std::string userInput;
	std::vector<int> integerTest;
	std::cout << "--== Number Palindrome ==--" << std::endl;
	std::cout << "Enter integers seperated by commas: e.g. 1,2,3,2,1" << std::endl;
	std::cin >> userInput;
	std::stringstream ss(userInput);
	int i;
	while (ss >> i)
	{
		integerTest.push_back(i);
		if (ss.peek() == ',')
			ss.ignore();
	}
	if (isPalindrome(integerTest))
	{
		std::cout << "Numbers are a Palindrome!" << std::endl;
	}
	else
	{
		std::cout << "Numbers are not a Palindrome!" << std::endl;
	}

	std::cout << std::endl;
}

void stringPalindrome()
{
	std::string userInput;
	std::vector<char> stringTest;
	std::cout << "--== String Palindrome ==--" << std::endl;
	std::cout << "Enter String:" << std::endl;
	std::cin >> userInput;
	std::copy(userInput.begin(), userInput.end(), std::back_inserter(stringTest));
	if (isPalindrome(stringTest))
	{
		std::cout << "String is a Palindrome!" << std::endl;
	}
	else
	{
		std::cout << "String is not a Palindrome!" << std::endl;
	}
	std::cout << std::endl;
}

int main(){
	
	int input;
	
	do{
		std::cout << "--== Palindrome Menu ==--" << std::endl;
		std::cout << "  1. Number Palindrome" << std::endl;
		std::cout << "  2. String Palindrome" << std::endl;
		std::cout << "  0. Exit" << std::endl;
		cin >> input;
		switch(input)
		{
		case 1:
			numberPalindrome();
			break;
		case 2:
			stringPalindrome();
			break;
		default:
			break;
		}
	}while(input != 0);

}