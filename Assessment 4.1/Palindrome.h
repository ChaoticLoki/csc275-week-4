#pragma once
#include "StandardIncludes.h"

template <class T> bool isPalindrome(vector<T> input){
	//Flip the first half of the vector to make it easier to compare
	std::reverse(input.begin(), input.begin() + input.size() / 2);
	/*
		Create and compare two temporary vectors created from the first half of the input vector and the second half of the input vector.
	
		1,2,3,4,5
		creates
		Vector One: 1,2
		Vector Two: 4,5
	*/
    return vector<T>(input.begin(),input.begin()+input.size()/2) == vector<T>(input.end()-input.size()/2,input.end());
}